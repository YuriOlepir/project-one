import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {LocalStorageService} from 'ngx-webstorage';
import companies from '../../companies.json';

@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.sass']
})
export class NewCompanyComponent implements OnInit {

  public list;
  public companyName;
  public city;
  public address;
  public latitude: number;
  public longitude: number;

  constructor(
    private location: Location,
    private localSt: LocalStorageService
  ) { }

  ngOnInit() {
    this.list = this.localSt.retrieve('companies');
  }
  goBack(): void {
    this.location.back();
  }
  onAutocompleteSelected(result) {
    this.city = result.address_components.filter(obj => {
      return obj.types[0] === 'locality';
    })[0].short_name || result.address_components.filter(obj => {
      return obj.types[0] === 'country';
    })[0].short_name;
    const route = result.address_components.filter(obj => {
      return obj.types[0] === 'route';
    })[0] || null;
    const streetNumber = result.address_components.filter(obj => {
      return obj.types[0] === 'street_number';
    })[0] || null;
    this.address = `${route ? route.short_name : ''} ${streetNumber ? streetNumber.short_name : ''}`;
  }

  onLocationSelected(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  saveCompany() {
    this.list.push(
      {
        name: this.companyName,
        city: this.city,
        address: this.address,
        latitude: this.latitude,
        longitude: this.longitude
      });
    this.localSt.store('companies', this.list);
    this.location.back();
  }
}

