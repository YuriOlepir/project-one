import {Component, HostListener, OnInit} from '@angular/core';
import companies from '../../companies.json';
import {LocalStorageService} from 'ngx-webstorage';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.sass']
})
export class CompaniesComponent implements OnInit {
  list = [];
  i = 0;
  toggleButton;
  page = [];
  wasScrolledToEnd: boolean;
  className = 'show-header';
  prevScrollpos: number;

  constructor(private localSt: LocalStorageService) {}

  ngOnInit() {
    this.prevScrollpos = 0;

    this.list = this.localSt.retrieve(`companies`).reduce((rows, key, index) =>
      (index % 10 === 0 ? rows.push([key])
      : rows[rows.length - 1].push(key)) && rows, []);
    this.getItems();
    this.wasScrolledToEnd = true;
  }

  @HostListener('window:scroll', ['$event'])
  scrollMe(event) {
    const scrollTop = event.target.scrollingElement.scrollTop;
    if (this.prevScrollpos < scrollTop || scrollTop < 64) {
      this.className = 'hide-header';
    } else {
      this.className = 'show-header';
    }
    this.prevScrollpos = scrollTop;
    const scrollHeight = event.target.scrollingElement.scrollHeight;
    const wh = window.innerHeight;
    const isScrolledToEnd = scrollTop > (scrollHeight - wh - 200);
    if (!this.wasScrolledToEnd && isScrolledToEnd) {
      this.wasScrolledToEnd = true;
      this.getItems();
    } else if (!isScrolledToEnd) {
      this.wasScrolledToEnd = false;
    }
  }
  getItems() {
    this.page.push(...this.list[this.i]);
    this.i = this.i + 1;
  }
  removeItem(index) {
    this.page.splice(index, 1);
    this.localSt.store('companies', this.page);
  }
  toggleMenu() {
    this.toggleButton = !this.toggleButton;
  }

  clearList() {
    this.toggleButton = false;
    this.page = [];
    this.list = [];
    this.localSt.store('companies', this.page);
  }
}
