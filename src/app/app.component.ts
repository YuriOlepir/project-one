import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from 'ngx-webstorage';
import companies from '../companies.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  page;
  constructor(private localSt: LocalStorageService) {}

  ngOnInit() {
    this.page = companies.companies.reduce((rows, key, index) => (index % 5 == 0 ? rows.push([key])
      : rows[rows.length - 1].push(key)) && rows, []);
    this.localSt.store('companies', companies.companies);
  }
}
