import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import companies from '../../companies.json';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.sass']
})
export class CompanyComponent implements OnInit {
  company;
  lat;
  lng;
  constructor(
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.company = companies.companies.filter(obj => {
      return obj.name === id;
    })[0];
    this.lat = parseFloat(this.company.latitude);
    this.lng = parseFloat(this.company.longitude);

  }
  goBack(): void {
    this.location.back();
  }
}
